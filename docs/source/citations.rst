Citations
------------

If you use HiFAST and its modules in your research, please consider citing the following papers associated with the respective modules. Each of these papers details specific aspects of the HiFAST pipeline.

1.  **The HiFAST Core Pipeline:**

    HiFAST: an HI data calibration and imaging pipeline for FAST. `DOI <https://doi.org/10.1007/s11433-023-2333-8>`_  | `arXiv <https://arxiv.org/abs/2401.17364>`_ | `ADS <https://ui.adsabs.harvard.edu/abs/2024SCPMA..6759514J>`_

    This paper describes the overall structure and fundamental aspects of the HiFAST pipeline.
    
2.  **Flux Density Calibration Module:**

    HiFAST: An HI Data Calibration and Imaging Pipeline for FAST II. Standing Wave Removal. `DOI <https://doi.org/10.1088/1674-4527/ad5398>`_ | `arXiv <https://arxiv.org/abs/2406.08278>`_ | `ADS <https://ui.adsabs.harvard.edu/abs/2024RAA....24h5009L/abstract>`_

    This paper describes the flux density calibration methods used in the HiFAST pipeline, including the utilization of calibrator observations and pre-measured gain values.
    
3.  **Standing Wave Removal Module:**

    HiFAST: An HI Data Calibration and Imaging Pipeline for FAST III. Standing Wave Removal. `DOI <https://doi.org/10.1088/1674-4527/ad9653>`_ | `arXiv <https://arxiv.org/abs/2411.13016>`_ | `ADS <https://ui.adsabs.harvard.edu/abs/2024arXiv241113016X/abstract>`_

    This paper presents the techniques employed by the HiFAST pipeline for removing standing waves from HI data, including the FFT-filter approach.

.. 4. **Stray Radiation Correction Module:**

.. This paper provides details on the correction of the stray radiation effects.